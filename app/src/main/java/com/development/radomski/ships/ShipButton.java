package com.development.radomski.ships;

import android.content.Context;
import android.widget.ImageButton;


public class ShipButton extends ImageButton {

    public static int BOARD_BUTTON_SIZE = 23;
    public static int BOARD_BUTTON_MARGIN = 1;

    private int posX;
    private int posY;
    private ShipButtonStatus status;

    public ShipButton(Context context, int posX, int posY, ShipButtonStatus status) {
        super(context);
        this.posX = posX;
        this.posY = posY;
        this.status = status;
        float density = getResources().getDisplayMetrics().density;
        setMinimumWidth((int) (BOARD_BUTTON_SIZE * density));
        setMinimumHeight((int) (BOARD_BUTTON_SIZE * density));
    }

    public boolean isConnectedHorizontaly(ShipButton shipButton) {
        if (this.posX + 1 != shipButton.posX) {
            if (this.posX - 1 != shipButton.posX) return false;
        }
        return true;
    }

    public boolean isConnectedVerticaly(ShipButton shipButton) {
        if (this.posY + 1 != shipButton.posY) {
            if (this.posY - 1 != shipButton.posY) return false;
        }
        return true;
    }

    public boolean isRightConnected(ShipButton shipButton) {
        return this.posX+1 == shipButton.posX;
    }

    public boolean isDownConnected(ShipButton shipButton) {
        return this.posY-1 == shipButton.posX;
    }
}
