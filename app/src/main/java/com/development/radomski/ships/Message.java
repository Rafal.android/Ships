package com.development.radomski.ships;

import java.util.Date;


public class Message {
    public static final int STATUS_MINE = 0;
    public static final int STATUS_DELIVERED = 1;
    public static final int STATUS_RECIEVED = 2;
    public static final int STATUS_SEND_FALIURE = 3;

    private String mess;
    private int status;
    private Date date;

    public Message(String mess, int status, Date date) {
        this.mess = mess;
        this.status = status;
        this.date = date;
    }

    public String getMess() {
        return mess;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isMine() {
        return status == 0 ? true : false;
    }
    public boolean isDelivered(){
        return status == 1 ? true : false;
    }
    public boolean isRecieved(){
        return status == 2 ? true : false;
    }
}
