package com.development.radomski.ships;



import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SelectNewGameActivity extends AppCompatActivity {

    Button buttonHostGame;
    Button buttonJoinGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_new_game);

        buttonHostGame = (Button)findViewById(R.id.buttonHostGame);
        buttonHostGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hostGame(v);
            }
        });
        buttonJoinGame = (Button)findViewById(R.id.buttonJoinGame);
        buttonJoinGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinGame();
            }
        });
    }

    public void hostGame(final View v){
        AsyncHostGameStart asyncHostGameStart = new AsyncHostGameStart();
        asyncHostGameStart.execute();
    }
    public void joinGame(){

    }

    class AsyncHostGameStart extends AsyncTask<Object, Object, Object> {

        public AsyncHostGameStart() {
            super();
        }

        public MyProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new MyProgressDialog(SelectNewGameActivity.this);
            progressDialog.show();

        }

        @Override
        protected Object doInBackground(Object... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            Intent intent = new Intent(getApplicationContext(),GameActivity.class);
            startActivity(intent);
            progressDialog.dismiss();
            finish();
        }

    }

}
