package com.development.radomski.ships;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {

    Button buttonNewGame;
    Button buttonCredits;
    Button buttonExit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        buttonNewGame = (Button) findViewById(R.id.buttonNewGame);
        buttonNewGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectNewGame();
            }
        });
        buttonCredits = (Button) findViewById(R.id.buttonCredits);
        buttonCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCredits();
            }
        });
        buttonExit = (Button) findViewById(R.id.buttonExit);
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit();
            }
        });
    }

    public void selectNewGame(){
        Intent intentSelectNewGame = new Intent(this, SelectNewGameActivity.class);
        startActivity(intentSelectNewGame);
    }
    public void goToCredits() {
        Intent intentGoToCredits = new Intent(this, CreditsActivity.class);
        startActivity(intentGoToCredits);
    }
    public void exit() {
        this.finish();
    }
}
