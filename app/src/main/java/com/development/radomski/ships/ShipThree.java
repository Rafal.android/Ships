package com.development.radomski.ships;

public class ShipThree extends Ship {

    private ShipButton shipButtonLeft;
    private ShipButton shipButtonMiddle;
    private ShipButton shipButtonRight;

    public ShipThree(ShipButton shipButtonLeft, ShipButton shipButtonMiddle, ShipButton shipButtonRight) {
        this.shipButtonLeft = shipButtonLeft;
        this.shipButtonMiddle = shipButtonMiddle;
        this.shipButtonRight = shipButtonRight;
    }
}
