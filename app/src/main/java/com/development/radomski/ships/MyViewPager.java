package com.development.radomski.ships;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class MyViewPager extends ViewPager {
    private boolean swipeable = true;

    public boolean isSwipeable() {
        return swipeable;
    }

    public void setSwipeable(boolean scrollable) {
        this.swipeable = scrollable;
    }

    public MyViewPager(Context context) {
        super(context);
    }

    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return swipeable && super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return swipeable && super.onTouchEvent(ev);
    }

    @Override
    public boolean canScrollHorizontally(int direction) {
        return swipeable && super.canScrollHorizontally(direction);
    }
}
