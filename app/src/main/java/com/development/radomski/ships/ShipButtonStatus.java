package com.development.radomski.ships;


public enum ShipButtonStatus {
    WATER,
    SHIP_SWIM,
    SHIP_SINKED,
    MISSED,
}
