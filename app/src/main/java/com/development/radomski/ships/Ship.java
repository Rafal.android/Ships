package com.development.radomski.ships;

public class Ship {
    public final static int SHIP_ONE_NUMBER = 4;
    public final static int SHIP_TWO_NUMBER = 3;
    public final static int SHIP_THREE_NUMBER = 2;
    public final static int SHIP_FOUR_NUMBER = 1;

    protected boolean isHorizontal;

    public boolean isHorizontal(ShipButton shipButton, ShipButton shipButton2){
        isHorizontal = shipButton.isConnectedHorizontaly(shipButton2);
        return isHorizontal;
    }
}
