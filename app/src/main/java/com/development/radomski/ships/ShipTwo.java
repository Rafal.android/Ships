package com.development.radomski.ships;

public class ShipTwo extends Ship {

    private ShipButton shipButtonLeft;
    private ShipButton shipButtonRight;

    public ShipTwo(ShipButton shipButtonLeft, ShipButton shipButtonRight) {
        this.shipButtonLeft = shipButtonLeft;
        this.shipButtonRight = shipButtonRight;
    }
}
