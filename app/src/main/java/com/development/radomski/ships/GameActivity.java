package com.development.radomski.ships;

import android.content.Context;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

public class GameActivity extends FragmentActivity {

    public static int BOARD_SIZE = 10;

    private boolean boardClicable = false;
    private MyBoard myBoard;
    private EnemyBoard enemyBoard;
    private Chat chat;

    private GamePagerAdapter gamePagerAdapter;

    private MyViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        myBoard = new MyBoard();
        enemyBoard = new EnemyBoard();
        chat = new Chat();
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        gamePagerAdapter = new GamePagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.

        viewPager = (MyViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(gamePagerAdapter);

        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
//                Looper.loop();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                beginGame();
            }
        });
        thread.start();
    }

    public void lockPager() {
        viewPager.setSwipeable(false);
    }

    public void unlockPager() {
        viewPager.setSwipeable(true);
    }

    public void beginGame() {
        lockPager();
        myBoard.placeShips();
    }

    public static class EnemyBoard extends Board {

    }

    public static class MyBoard extends Board {

        private int shipCount = 0;
        private int masterCount = 0;
        private boolean tmpIsHorizontal;


        public void placeShips() {
            Toast toast = Toast.makeText(getContext(), "Place Your ships.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.BOTTOM, 1, 1);
            toast.show();
            gridLayout.setOnClickListener(new View.OnClickListener() {
                                              @Override
                                              public void onClick(View v) {
                                                  ShipButton ttmp;
                                                  switch (shipCount) {
                                                      case 0:
                                                          switch (masterCount) {
                                                              case 0:
                                                                  tmp.add((ShipButton) v);
                                                                  masterCount++;
                                                                  break;
                                                              case 1:
                                                                  ttmp = ((ShipButton) v);
                                                                  if (ttmp.isConnectedHorizontaly(tmp.get(0))) {
                                                                      tmpIsHorizontal = true;
                                                                      if (ttmp.isRightConnected(tmp.get(0))) {
                                                                          tmp.add(0, ttmp);
                                                                      } else {
                                                                          tmp.add(ttmp);
                                                                      }
                                                                  } else if (ttmp.isConnectedVerticaly(tmp.get(0))) {
                                                                      tmpIsHorizontal = false;
                                                                      if (ttmp.isDownConnected(tmp.get(0))) {
                                                                          tmp.add(0, ttmp);
                                                                      } else {
                                                                          tmp.add(ttmp);
                                                                      }
                                                                  } else {
                                                                      shipsNotConnectedError();
                                                                      break;
                                                                  }
                                                                  masterCount++;
                                                                  break;
                                                              case 2:
                                                                  ttmp = (ShipButton) v;
                                                                  if (tmpIsHorizontal) {
                                                                      if (!ttmp.isConnectedHorizontaly(tmp.get(1))) {
                                                                          if (!ttmp.isConnectedHorizontaly(tmp.get(0))) {
                                                                              shipsNotConnectedError();
                                                                              break;
                                                                          } else {
                                                                              tmp.add(0, ttmp);
                                                                          }
                                                                      } else {
                                                                          tmp.add(ttmp);
                                                                      }
                                                                  } else if (!ttmp.isConnectedHorizontaly(tmp.get(1))) {
                                                                      if (!ttmp.isConnectedHorizontaly(tmp.get(0))) {
                                                                          shipsNotConnectedError();
                                                                          break;
                                                                      } else {
                                                                          tmp.add(0, ttmp);
                                                                      }
                                                                  } else {
                                                                      tmp.add(ttmp);
                                                                  }
                                                                  masterCount++;
                                                                  break;
                                                              case 3:
                                                                  ttmp = (ShipButton) v;
                                                                  if (tmpIsHorizontal) {
                                                                      if (ttmp.isConnectedHorizontaly(tmp.get(2))) {
                                                                          shipFour[shipCount] = new ShipFour(tmp.get(0), tmp.get(1), tmp.get(2), ttmp);
                                                                      } else if (ttmp.isConnectedHorizontaly(tmp.get(0))) {
                                                                          shipFour[shipCount] = new ShipFour(ttmp, tmp.get(0), tmp.get(1), tmp.get(2));
                                                                      } else {
                                                                          shipsNotConnectedError();
                                                                          break;
                                                                      }
                                                                  } else if (ttmp.isConnectedVerticaly(tmp.get(2))) {
                                                                      shipFour[shipCount] = new ShipFour(tmp.get(0), tmp.get(1), tmp.get(2), ttmp);
                                                                  } else if (ttmp.isConnectedVerticaly(tmp.get(0))) {
                                                                      shipFour[shipCount] = new ShipFour(ttmp, tmp.get(0), tmp.get(1), tmp.get(2));
                                                                  } else {
                                                                      shipsNotConnectedError();
                                                                      break;
                                                                  }
                                                                  shipFour[shipCount].draw(tmpIsHorizontal);
                                                                  masterCount = 0;
                                                                  shipCount++;
                                                                  break;
                                                          }
//                                                      case Ship.SHIP_FOUR_NUMBER:
//                                                          switch (masterCount) {
//                                                              case 0:
//                                                                  tmp.add((ShipButton) v);
//                                                                  masterCount++;
//                                                                  break;
//                                                              case 1:
//                                                                  ttmp = ((ShipButton) v);
//                                                                  if (ttmp.isConnectedHorizontaly(tmp.get(0))) {
//                                                                      tmpIsHorizontal = true;
//                                                                  } else if (ttmp.isConnectedVerticaly(tmp.get(0))) {
//                                                                      tmpIsHorizontal = false;
//                                                                  } else {
//                                                                      shipsNotConnectedError();
//                                                                      break;
//                                                                  }
//                                                                  masterCount++;
//                                                                  break;
//                                                              case 2:
//                                                                  ttmp = (ShipButton) v;
//                                                                  if (tmpIsHorizontal) {
//                                                                      if (!ttmp.isConnectedHorizontaly(tmp.get(1))) {
//                                                                          if (!ttmp.isConnectedHorizontaly(tmp.get(0))) {
//                                                                              shipsNotConnectedError();
//                                                                              break;
//                                                                          } else {
//                                                                              tmp.add(ttmp);
//                                                                          }
//                                                                      } else {
//                                                                          tmp.add(tmp.size(), ttmp);
//                                                                      }
//                                                                  } else if (!ttmp.isConnectedHorizontaly(tmp.get(1))) {
//                                                                      if (!ttmp.isConnectedHorizontaly(tmp.get(0))) {
//                                                                          shipsNotConnectedError();
//                                                                          break;
//                                                                      } else {
//                                                                          tmp.add(ttmp);
//                                                                      }
//                                                                  } else {
//                                                                      tmp.add(tmp.size(), ttmp);
//                                                                  }
//                                                                  masterCount++;
//                                                                  break;
//                                                              case 3:
//                                                                  ttmp = (ShipButton) v;
//                                                                  if (tmpIsHorizontal) {
//                                                                      if (ttmp.isConnectedHorizontaly(tmp.get(2))) {
//                                                                          shipFour[shipCount] = new ShipFour(tmp.get(0), tmp.get(1), tmp.get(2), ttmp);
//                                                                      } else if (ttmp.isConnectedHorizontaly(tmp.get(0))) {
//                                                                          shipFour[shipCount] = new ShipFour(ttmp, tmp.get(0), tmp.get(1), tmp.get(2));
//                                                                      } else {
//                                                                          shipsNotConnectedError();
//                                                                          break;
//                                                                      }
//                                                                  } else if (ttmp.isConnectedVerticaly(tmp.get(2))) {
//                                                                      shipFour[shipCount] = new ShipFour(tmp.get(0), tmp.get(1), tmp.get(2), ttmp);
//                                                                  } else if (ttmp.isConnectedVerticaly(tmp.get(0))) {
//                                                                      shipFour[shipCount] = new ShipFour(ttmp, tmp.get(0), tmp.get(1), tmp.get(2));
//                                                                  } else {
//                                                                      shipsNotConnectedError();
//                                                                      break;
//                                                                  }
//                                                                  shipFour[shipCount].draw(tmpIsHorizontal);
//                                                                  masterCount = 0;
//                                                                  shipCount++;
//                                                                  break;
//                                                          }
                                                  }
                                              }
                                          }
            );

        }

        private void shipsNotConnectedError() {
            Toast toast = Toast.makeText(getContext(), "Ship You picked is not connected properly to ships, that You picked earlier! Pick another one.", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.BOTTOM, 0, 0);
            toast.show();
        }

//        @Override
//        public void onResume() {
//            super.onResume();
//            Toast toast = Toast.makeText(getContext(),"Place Your ships.",Toast.LENGTH_LONG);
//            toast.setGravity(Gravity.BOTTOM,0,0);
//            toast.show();
//        }
    }


    public static class Board extends Fragment {

        protected ArrayList<ShipButton> tmp = new ArrayList<>(2);
        protected ShipButton[][] board = new ShipButton[GameActivity.BOARD_SIZE][GameActivity.BOARD_SIZE];
        protected ShipOne[] shipOne = new ShipOne[Ship.SHIP_ONE_NUMBER];
        protected ShipTwo[] shipTwo = new ShipTwo[Ship.SHIP_TWO_NUMBER];
        protected ShipThree[] shipThree = new ShipThree[Ship.SHIP_THREE_NUMBER];
        protected ShipFour[] shipFour = new ShipFour[Ship.SHIP_FOUR_NUMBER];
        protected GridLayout gridLayout;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_game, container, false);
            gridLayout = (GridLayout) view.findViewById(R.id.gridLayout);
            gridLayout.setRowOrderPreserved(true);
            for (int i = 0; i < GameActivity.BOARD_SIZE; i++) {
                board[i] = new ShipButton[BOARD_SIZE];
                for (int j = 0; j < GameActivity.BOARD_SIZE; j++) {
                    board[i][j] = new ShipButton(this.getContext(), i, j, ShipButtonStatus.WATER);
                    setShipButtonInGridLayout(gridLayout, board[i][j], i + 1, j + 1, ShipButton.BOARD_BUTTON_MARGIN, R.drawable.blank_square, ShipButton.BOARD_BUTTON_SIZE, ShipButton.BOARD_BUTTON_SIZE);
                }
            }
            return view;
        }

        private void setImageViewInGridLayout(GridLayout gridLayout, ImageView imageView, int row, int column, int margin, int backgroundRes, int wifthInDP, int heightInDP) {
            GridLayout.Spec specRow;
            GridLayout.Spec specColumn;
            GridLayout.LayoutParams layoutParams;
            float density = getActivity().getResources().getDisplayMetrics().density;
            specRow = GridLayout.spec(row, 1);
            specColumn = GridLayout.spec(column, 1);
            layoutParams = new GridLayout.LayoutParams(specRow, specColumn);
            layoutParams.setMargins((int) (margin * density), (int) (margin * density), (int) (margin * density), (int) (margin * density));
            layoutParams.setGravity(Gravity.TOP);
            imageView.setImageResource(backgroundRes);
            imageView.setLayoutParams(layoutParams);
            imageView.setMaxWidth((int) (wifthInDP * density));
            imageView.setMaxHeight((int) (heightInDP * density));
            gridLayout.addView(imageView);
        }

        private void setShipButtonInGridLayout(GridLayout gridLayout, ShipButton shipButton, int row, int column, int margin, int backgroundRes, int wifthInDP, int heightInDP) {
            GridLayout.Spec specRow;
            GridLayout.Spec specColumn;
            GridLayout.LayoutParams layoutParams;
            float density = getActivity().getResources().getDisplayMetrics().density;
            specRow = GridLayout.spec(row, 1);
            specColumn = GridLayout.spec(column, 1);
            layoutParams = new GridLayout.LayoutParams(specRow, specColumn);
            layoutParams.setMargins((int) (margin * density), (int) (margin * density), (int) (margin * density), (int) (margin * density));
            layoutParams.setGravity(Gravity.CENTER);
            shipButton.setBackgroundResource(backgroundRes);
            shipButton.setLayoutParams(layoutParams);
            shipButton.setMaxWidth((int) (wifthInDP * density));
            shipButton.setMaxHeight((int) (heightInDP * density));
            gridLayout.addView(shipButton);
        }

    }

    public static class Chat extends Fragment {
        private ArrayList<Message> messages;
        private ListAdapter adp;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.chat, container, false);

            messages = new ArrayList<>();
            adp = new ChatAdapter(inflater);

            final ListView listView = (ListView) view.findViewById(R.id.chat_list);

            listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
            listView.setStackFromBottom(true);
            listView.setAdapter(adp);

            final EditText editText = (EditText) view.findViewById(R.id.input_message);
            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);

            Button button = (Button) view.findViewById(R.id.button_send);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editText.getText().length() != 0) {
                        messages.add(new Message(editText.getText().toString(), Message.STATUS_MINE, new Date()));
                        editText.setText("");
                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                }
            });
            return view;
        }

        private class ChatAdapter extends BaseAdapter implements ListAdapter {
            LayoutInflater inflater;

            public ChatAdapter(LayoutInflater inflater) {
                super();
                this.inflater = inflater;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                Message message = getItem(position);
                if (!message.isMine()) {
                    convertView = inflater.inflate(R.layout.message_recieved, parent, false);
                } else {
                    convertView = inflater.inflate(R.layout.message_send, parent, false);
                }
                TextView textView = (TextView) convertView.findViewById(R.id.message_recieved_time);
                textView.setText(message.getDate().toString());
                textView = (TextView) convertView.findViewById(R.id.message_recieved_text);
                textView.setText(message.getMess());
                textView = (TextView) convertView.findViewById(R.id.message_recieved_status);
                if (message.isMine()) {
                    textView.setText("Sending...");
                } else if (message.isDelivered()) {
                    textView.setText("Delivered");
                } else if (message.isRecieved()) {
                    textView.setText("Recieved");
                } else textView.setText("Failed");
                return convertView;
            }

            @Override
            public Message getItem(int position) {
                return messages.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public int getCount() {
                return messages.size();
            }
        }
    }

    private class GamePagerAdapter extends FragmentPagerAdapter {


        public GamePagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return myBoard;
                case 1:
                    return chat;
                case 2:
                    return enemyBoard;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "My Board";
                case 1:
                    return "Chat";
                case 2:
                    return "Enemy's Board";
            }
            return null;
        }
    }
}
