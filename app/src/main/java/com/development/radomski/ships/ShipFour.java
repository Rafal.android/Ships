package com.development.radomski.ships;

public class ShipFour extends Ship {

    private ShipButton shipButtonLeftOrUp;
    private ShipButton shipButtonMiddleLeftOrUp;
    private ShipButton shipButtonMiddleRightOrDown;
    private ShipButton shipButtonRightOrDown;

    public ShipFour(ShipButton shipButtonLeftOrUp, ShipButton shipButtonMiddleLeftOrUp, ShipButton shipButtonMiddleRightOrDown, ShipButton shipButtonRightOrDown) {
        this.shipButtonLeftOrUp = shipButtonLeftOrUp;
        this.shipButtonMiddleLeftOrUp = shipButtonMiddleLeftOrUp;
        this.shipButtonMiddleRightOrDown = shipButtonMiddleRightOrDown;
        this.shipButtonRightOrDown = shipButtonRightOrDown;
    }

    public void draw(boolean isHorizontal) {
        this.isHorizontal = isHorizontal;
        if (this.isHorizontal) {
            shipButtonLeftOrUp.setBackgroundResource(R.drawable.ship_four_square_horizontal_swim_left);
            shipButtonMiddleLeftOrUp.setBackgroundResource(R.drawable.ship_four_square_horizontal_swim_middle);
            shipButtonMiddleRightOrDown.setBackgroundResource(R.drawable.ship_four_square_horizontal_swim_middle);
            shipButtonRightOrDown.setBackgroundResource(R.drawable.ship_four_square_horizontal_swim_right);
        } else{
            shipButtonLeftOrUp.setBackgroundResource(R.drawable.ship_four_square_vertical_swim_up);
            shipButtonMiddleLeftOrUp.setBackgroundResource(R.drawable.ship_four_square_vertical_swim_middle);
            shipButtonMiddleRightOrDown.setBackgroundResource(R.drawable.ship_four_square_vertical_swim_middle);
            shipButtonRightOrDown.setBackgroundResource(R.drawable.ship_four_square_vertical_swim_down);
        }
    }
}
